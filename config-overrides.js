// import darkTheme from '@ant-design/dark-theme'
// addWebpackModuleRule
const { override, addWebpackAlias } = require('customize-cra')
const path = require('path')

module.exports = override(
  addWebpackAlias({ '@': path.resolve(__dirname, 'src') })
)
