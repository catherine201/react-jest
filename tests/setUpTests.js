import 'raf/polyfill' // 安装一下requestAnimation的模拟,react需要
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import * as jsdom from 'jsdom'
import jQuery from '../static/thirdjs/jquery-1.12.4.min.js' // 如果项目使用了jquery
import 'whatwg-fetch'

const { window } = new jsdom.JSDOM('<!doctype html><html><body></body></html>')

global.window = window
global.document = window.document
window.$ = window.jQuery = jQuery
global.$ = global.jQuery = jQuery
// global.jsbarcode = global.jsbarcode = {}
global.console = {
  log: console.log,
  warn: jest.fn(),
  error: console.error,
  info: console.info,
  debug: console.debug
}
configure({ adapter: new Adapter() })
