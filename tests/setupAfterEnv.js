import { toMatchMountSnapshot, toMatchShallowSnapshot } from './matchers/rendered-snapshot'

expect.extend({
  toMatchMountSnapshot,
  toMatchShallowSnapshot
})
