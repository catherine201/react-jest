import React from 'react'
import { mount } from 'enzyme'
import App from './App'

test('snap', () => {
  const wrapper = mount(<App />)
  expect(wrapper.render()).toMatchSnapshot()
})
