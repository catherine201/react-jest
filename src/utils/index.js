/**
 * 获取字符串中某个匹配字符串的个数
 * @param {*} scrstr 源字符串
 * @param {*} armstr 匹配字符
 * @param {*} ignoreI  是否忽略大小写，默认为忽略大小写
 */
export function getStrCount (scrstr, armstr, ignoreI = true) {
  const needEscapeChar = ['$', '(', ')', '*', '+', '.', '[', ']', '?', '^', '{', '}', '|']
  const result = scrstr.match(
    new RegExp(needEscapeChar.indexOf(armstr) > -1 ? `\\${armstr}` : armstr, `g${ignoreI ? 'i' : ''}`)
  )
  return result ? result.length : 0
}
